tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : ((e=expr {drukuj ($e.text + " = " + $e.out.toString());}) | deklr)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = dodawanie($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = odejmowanie($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mnozenie($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = dzielenie($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setVar($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVar($ID.text);}
        ;

deklr : ^(VAR i1=ID) {declareVar($i1.text);} ;